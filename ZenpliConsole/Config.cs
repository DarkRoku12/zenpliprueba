﻿using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace ZenpliConsole
{
  // Singleton for JSON settings.
  public static class Settings
  {
    static private dynamic _settings;

    static public dynamic stx { get => _settings; }

    static public void Start()
    {
      var settingsTxt = System.IO.File.ReadAllText( "settings.json" );
      _settings = JsonConvert.DeserializeObject<dynamic>( settingsTxt );
    }
  }

  // Configure database with EntityFramework & NpgSQL (PostgreSQL).
  public class Database : DbContext
  {
    public DbSet<InputTable> Data { get; set; }
    protected override void OnConfiguring( DbContextOptionsBuilder optionsBuilder )
    {
      var stx = Settings.stx;
      var conn = @$"Host={stx.db_host};Port={stx.db_port};Username={stx.db_user};Password={stx.db_pass};Database={stx.db_name}";
      optionsBuilder.UseNpgsql( conn ).UseSnakeCaseNamingConvention();
      optionsBuilder.EnableSensitiveDataLogging(true);
      optionsBuilder.EnableDetailedErrors(true);
    }
  }
}
