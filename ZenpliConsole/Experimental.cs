﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;

namespace ZenpliConsole
{
  // For interview explanation only.
  class Experimental
  {
    public record my_row_in
    {
      public string Key_1 { get; init; }
      public DateTime Date_2 { get; init; }
      public float Cont_3 { get; init; }
      public float Cont_4 { get; init; }
    }

    public record my_column_out
    {
      public int Length { get; init; }
      public string[] Keys_1 { get; init; }
      public DateTime[] Dates_2 { get; init; }
      public Vector2[] Conts_3_4 { get; init; }
    }

    // Convert row-based to column-based...
    public static my_column_out ToColumnar( IList<my_row_in> arr )
    {
      var keys_1 = new string[ arr.Count ];
      var dates_2 = new DateTime[ arr.Count ];
      var counts_3_4 = new Vector2[ arr.Count ]; // https://docs.microsoft.com/en-us/dotnet/api/system.numerics.vector-1

      Parallel.For( 0 , arr.Count , index =>
      {
        var row = arr[ index ];
        keys_1[ index ] = row.Key_1;
        dates_2[ index ] = row.Date_2;
        counts_3_4[ index ] = new Vector2( row.Cont_3 , row.Cont_4 );
      } );

      return new my_column_out
      {
        Length = arr.Count ,
        Keys_1 = keys_1 ,
        Dates_2 = dates_2 ,
        Conts_3_4 = counts_3_4 ,
        // ... other fields.
      };
    }
  }
}
