﻿using System;
using System.Linq;

namespace ZenpliConsole
{
  static class Helpers
  {
    public struct MinMax<Type>
    {
      public Type Min { get; init; }
      public Type Max { get; init; }
    }

    public delegate Type RawValueGetter<Type>( string[] srcArray , uint index );

    // Simple and most optimal getter, can throw an exception if out of bounds.
    public static RawValueGetter<string> SimpleRawGetter = ( string[] srcArray , uint index ) =>
    {
      return srcArray[ index ];
    };

    // Safe way to get the default value (usually null) if index is out of bounds.
    public static RawValueGetter<string> DefaultRawGetter = ( string[] srcArray , uint index ) =>
    {
      return srcArray.ElementAtOrDefault( (int)index );
    };

    // Normalize a value according to min-max.
    public static decimal Normalize( decimal input , decimal min , decimal max )
    {
      return ( input - min ) / ( max - min );
    }
    
    // Computate min-max of a column.
    public static MinMax<OuputType> GetMinMax< InputTable, OuputType>( InputTable[] array , Func<InputTable , OuputType> callback )
    {
      return new MinMax<OuputType> 
      {
        Min = array.Min( callback ),
        Max = array.Max( callback ) ,
      };
    }
  }
}
