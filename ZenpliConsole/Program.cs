﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZenpliConsole
{
  class Program
  {
    static void Main( string[] args )
    {
      Console.WriteLine( "Zenpli - Data pipeline" );

      Settings.Start();

      #region Parse input
      var inputPath = (string)Settings.stx.input;
      var rawLines = System.IO.File.ReadAllLines( inputPath ); // Read the file to array of lines.
      #endregion

      var rawGetter = Helpers.DefaultRawGetter;

      #region Transform input to row set.
      var rowsUnsafe = rawLines
        .Skip( 1 ) // Skip header.
        .AsParallel() // Convert to PLinQ.
        .Select( line =>  // Convert lines to row-based table.
        {
          var values = line.Split( "," ); // Convert string to array of sub-strings.

          return new
          {
            line = line ,
            key_1 = rawGetter( values , 0 ) ,
            date_2 = rawGetter( values , 1 ) ,
            cont_3 = rawGetter( values , 2 ) ,
            cont_4 = rawGetter( values , 3 ) ,
            disc_5 = rawGetter( values , 4 ) ,
            disc_6 = rawGetter( values , 5 ) ,
            cat_7 = rawGetter( values , 6 ) ,
            cat_8 = rawGetter( values , 7 ) ,
            cont_9 = rawGetter( values , 8 ) ,
            cont_10 = rawGetter( values , 9 )
          };
        } )
        .Select( row => // Convert strings to types avoiding, defaulting wrong values to null.
        {
          try
          {
            return new InputTable
            {
              Key_1 = row.key_1 ,
              IsValid = true,
              Date_2 = DateTime.Parse( row.date_2 ) ,
              Cont_3 = decimal.Parse( row.cont_3 ) ,
              Cont_4 = decimal.Parse( row.cont_4 ) ,
              Disc_5 = int.Parse( row.disc_5 ) ,
              Disc_6 = int.Parse( row.disc_6 ) ,
              Cat_7 = row.cat_7 ,
              Cat_8 = row.cat_8 ,
              Cont_9 = decimal.Parse( row.cont_9 ) ,
              Cont_10 = decimal.Parse( row.cont_10 ) ,
            };
          }
          catch( Exception ex )
          {
            return new InputTable
            {
              Line = row.line ,
              IsValid = false ,
            };
          }
        } );
      #endregion

      #region Clasify/Filter row (valid ones and invalid).
      var badRows  = rowsUnsafe.Where( row => !row.IsValid );
      var goodRows = rowsUnsafe.Where( row => row.IsValid );
      #endregion

      #region Filtering by category (Cat_7 & Cat_8).
      var filter_cat_7 = new List<string>(); // { "never" };
      var filter_cat_8 = new List<string>(); // { "happy" };

      foreach( var f in Settings.stx.filters.cat_7 )
        filter_cat_7.Add( f.ToString() );

      foreach( var f in Settings.stx.filters.cat_8 )
        filter_cat_8.Add( f.ToString() );

      var filteredTable =
      !( filter_cat_7.Any() || filter_cat_8.Any() ) ?
      goodRows.ToArray() : // Fast path -> skip if no need to filter.
      goodRows.Where( row => // Slow path -> Need to apply filtering.
      {
        return
        filter_cat_7.Any() &&
        filter_cat_7.Contains( row.Cat_7 ) &&
        filter_cat_8.Any() &&
        filter_cat_8.Contains( row.Cat_8 );
      } ).ToArray();
      #endregion

      #region Normalize [Cont_3, Count_4, Cont_9, Cont_10]
      InputTable[] normalizedTable;

      {
        var min_max_3 = Helpers.GetMinMax( filteredTable , row => row.Cont_3 );
        var min_max_4 = Helpers.GetMinMax( filteredTable , row => row.Cont_4 );
        var min_max_9 = Helpers.GetMinMax( filteredTable , row => row.Cont_9 );
        var min_max_10 = Helpers.GetMinMax( filteredTable , row => row.Cont_10 );

        normalizedTable = filteredTable.Select( row =>
        {
          return row with
          {
            Norm_cont_3 = Helpers.Normalize( row.Cont_3 , min_max_3.Min , min_max_3.Max ) ,
            Norm_cont_4 = Helpers.Normalize( row.Cont_4 , min_max_4.Min , min_max_4.Max ) ,
            Norm_cont_9 = Helpers.Normalize( row.Cont_9 , min_max_9.Min , min_max_9.Max ) ,
            Norm_cont_10 = Helpers.Normalize( row.Cont_10 , min_max_10.Min , min_max_10.Max ) ,
          };
        } ).ToArray();
      }
      #endregion

      #region Transform [Cont_* + Norm_*] using: (x)^3 + exp(y)
      InputTable[] transformedTable;

      {
        transformedTable = normalizedTable.Select( row =>
        {
          return row with
          {
            TransNorm_3_4 = Math.Pow( (double)row.Norm_cont_3 , 3 ) + Math.Exp( (double)row.Norm_cont_4 ) ,
            TransNorm_9_10 = Math.Pow( (double)row.Norm_cont_9 , 3 ) + Math.Exp( (double)row.Norm_cont_10 ) ,
          };
        } ).ToArray();
      }
      #endregion

      #region Write error logs
      Console.WriteLine( $"Lines with errors = {badRows.Count()} | Writing error logs!..." );
      System.IO.File.WriteAllText( "bad-rows.txt" , string.Join( "\n" , badRows.Select( row => row.Line ) ) );
      #endregion

      #region Save into PostgreSQL
      using var db = new Database();
      Console.WriteLine( "Saving into PostgreSQL..." );
      db.AddRange( transformedTable );
      db.SaveChanges(); // Save previous changes.
      #endregion  

      Console.WriteLine( "Finished!...(waiting for key)" );
      Console.ReadLine();
    }
  }
}
