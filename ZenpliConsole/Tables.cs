﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ZenpliConsole
{
  [Table( "data" , Schema = "public" )]
  public record InputTable
  {
    [NotMapped]
    public string Line { get; init; }

    [NotMapped]
    public bool IsValid { get; init; }
    
    [Key]
    public long Id { get; init; }

    public string Key_1 { get; init; }
    public DateTime Date_2 { get; init; }
    public decimal Cont_3 { get; init; }

    public decimal Cont_4 { get; init; }
    public int Disc_5 { get; init; }
    public int Disc_6 { get; init; }
    public string Cat_7 { get; init; }
    public string Cat_8 { get; init; }
    public decimal Cont_9 { get; init; }
    public decimal Cont_10 { get; init; }

    // Normalized
    public decimal Norm_cont_3 { get; init; }
    public decimal Norm_cont_4 { get; init; }
    public decimal Norm_cont_9 { get; init; }
    public decimal Norm_cont_10 { get; init; }

    // Transformed
    public double TransNorm_3_4 { get; init; }
    public double TransNorm_9_10 { get; init; }
  }
}
